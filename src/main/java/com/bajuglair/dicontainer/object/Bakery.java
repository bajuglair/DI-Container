package com.bajuglair.dicontainer.object;

import com.bajuglair.dicontainer.annotation.Autowired;
import com.bajuglair.dicontainer.annotation.Component;
import com.bajuglair.dicontainer.annotation.Unique;
import com.bajuglair.dicontainer.equipement.Equipment;
import lombok.Data;

@Data
@Component
public class Bakery {

    @Autowired
    @Unique
    private Equipment fridge;

}
