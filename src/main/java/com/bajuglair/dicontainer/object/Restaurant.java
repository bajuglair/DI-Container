package com.bajuglair.dicontainer.object;

import com.bajuglair.dicontainer.annotation.Autowired;
import com.bajuglair.dicontainer.annotation.Component;
import com.bajuglair.dicontainer.annotation.Tag;
import com.bajuglair.dicontainer.equipement.Equipment;
import com.bajuglair.dicontainer.equipement.Fridge;
import com.bajuglair.dicontainer.equipement.Furnace;
import lombok.Data;



@Data
@Component
public class Restaurant {

    @Autowired
    @Tag(Fridge.class)
    private Equipment fridge;

    @Autowired
    @Tag(Furnace.class)
    private Equipment furnace;



}
