package com.bajuglair.dicontainer.container;

import com.bajuglair.dicontainer.annotation.Autowired;
import com.bajuglair.dicontainer.annotation.Tag;
import com.bajuglair.dicontainer.annotation.Unique;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Container {

    Map<Class<?>, Object> map = new HashMap<>();
    List<Relation> relations = new ArrayList<>();



    Relation getRelationBetween(Class<?> dependency, Class<?> abstraction){
        for(Relation r: relations){
            if(r.getDependency() == dependency && r.getAbstraction() == abstraction){
                return r;
            }
        }
        return null;
    }

    Relation getRelationOf(Class<?> dependency){
        for(Relation r: relations){
            if(r.getDependency() == dependency){
                return r;
            }
        }
        return null;
    }


    public void register(Class<?> dependency, Class<?> abstraction) {
        relations.add(new Relation(dependency, abstraction, null));
    }


    public Object newInstance(Class<?> dependency, int count, boolean unique) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        // No recursive infinite loop
        if(count > 20){
            return null;
        }

        // If the instance already exists and instance to create isn't unique
        if(!unique && map.get(dependency) != null ){
            return map.get(dependency);
        }

        // If it must be created
        else{
            Field[] fields = dependency.getDeclaredFields();
            Object instance =  dependency.getConstructor().newInstance();
            // For each field
            for(Field field : fields){
                field.setAccessible(true);
                // If field is Autowired
                if(field.isAnnotationPresent(Autowired.class)){
                    Class<?> c;
                    Object relationInstance;
                    Relation r;

                    if(field.isAnnotationPresent(Tag.class)){
                        c = field.getAnnotation(Tag.class).value();
                        r = getRelationBetween(field.getType(), c);
                        if(r == null){
                            register(field.getType(), c);
                            r = getRelationBetween(field.getType(), c);
                        }
                    }
                    else{
                        r = getRelationOf(field.getType());
                        if(r == null){
                            return null;
                        }
                        c = r.getAbstraction();
                    }

                    relationInstance = r.getInstance();

                    // Instance must be unique or already exists
                    if(!field.isAnnotationPresent(Unique.class) && relationInstance != null){
                        field.set(instance, r.getInstance());
                    }
                    else{
                        // If not, create it
                        Object obj = newInstance(c, count+1, field.isAnnotationPresent(Unique.class));

                        if(!field.isAnnotationPresent(Unique.class)){
                            r.setInstance(obj);
                        }

                        // Inject with set
                        field.set(instance, obj);
                    }

                }
            }
            map.put(dependency, instance);
            return instance;
        }
    }
}
