package com.bajuglair.dicontainer.container;


public class Relation {
    private Class<?> dependency;
    private Class<?> abstraction;
    private Object instance;

    public Relation(Class<?> dependency, Class<?> abstraction, Object instance){
        this.dependency = dependency;
        this.abstraction = abstraction;
        this.instance = instance;
    }

    public Class<?> getDependency(){
        return dependency;
    }

    public Class<?> getAbstraction(){
        return abstraction;
    }

    public Object getInstance(){
        return instance;
    }
    public void setInstance(Object instance) { this.instance = instance;}
}
