package com.bajuglair.dicontainer.equipement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class Equipment {
    String id;
    String name;


    void show(){
        System.out.println(name + " " + id);
    }

}
