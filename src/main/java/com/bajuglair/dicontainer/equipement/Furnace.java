package com.bajuglair.dicontainer.equipement;

import com.bajuglair.dicontainer.annotation.Component;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class Furnace extends Equipment {
    String type;

    @Override
    void show(){
        System.out.println(name + " " + id + " " + type);
    }

}
