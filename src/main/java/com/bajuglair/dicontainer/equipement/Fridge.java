package com.bajuglair.dicontainer.equipement;

import com.bajuglair.dicontainer.annotation.Autowired;
import com.bajuglair.dicontainer.annotation.Component;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class Fridge extends Equipment {
    String capacity;

    @Autowired
    Engine engine;

    @Override
    void show(){
        System.out.println(name + " " + id + " " + capacity);
    }
}
