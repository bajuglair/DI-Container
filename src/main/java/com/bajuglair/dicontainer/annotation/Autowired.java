package com.bajuglair.dicontainer.annotation;

// Code from https://dev.to/jjbrt/how-to-create-your-own-dependency-injection-framework-in-java-4eaj

import java.lang.annotation.*;
import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Injectable annotation
 */
@Target({ FIELD })
@Retention(RUNTIME)
@Documented
public @interface Autowired {

}