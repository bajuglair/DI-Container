package com.bajuglair.dicontainer.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Specify type to inject
 */
@Target({ FIELD })
@Retention(RUNTIME)
@Documented
public @interface Tag {
    Class<?> value();
}
