package com.bajuglair.dicontainer.annotation;

// Code from https://dev.to/jjbrt/how-to-create-your-own-dependency-injection-framework-in-java-4eaj

import java.lang.annotation.*;

/**
 * Client class should use this annotation
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Component {

}