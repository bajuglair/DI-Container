package com.bajuglair.dicontainer.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Unique injection required
 */
@Target({ FIELD })
@Retention(RUNTIME)
@Documented
public @interface Unique {

}
