package com.bajuglair.dicontainer;

import com.bajuglair.dicontainer.container.Container;
import com.bajuglair.dicontainer.equipement.Engine;
import com.bajuglair.dicontainer.equipement.Furnace;
import com.bajuglair.dicontainer.object.Bakery;
import com.bajuglair.dicontainer.equipement.Equipment;
import com.bajuglair.dicontainer.equipement.Fridge;
import com.bajuglair.dicontainer.object.BasicRestaurant;
import com.bajuglair.dicontainer.object.Restaurant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;

import static org.junit.jupiter.api.Assertions.*;

public class ContainerTest {

    Container container;

    @BeforeEach
    void createInjector() {
        container = new Container();
    }

    @Test
    @DisplayName("Test simple injection")
    void testSimpleInjection() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        container.register(Equipment.class, Fridge.class);
        container.register(Engine.class, Engine.class);

        BasicRestaurant basicRestaurant = (BasicRestaurant) container.newInstance(BasicRestaurant.class, 0, false);
        assertTrue(basicRestaurant.getFridge() instanceof Fridge);

        BasicRestaurant basicRestaurant2 = (BasicRestaurant) container.newInstance(BasicRestaurant.class, 0, false);
        assertSame(basicRestaurant2, basicRestaurant);

    }


    @Test
    @DisplayName("Test unique injection")
    void testUniqueInjection() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {

        container.register(Equipment.class, Fridge.class);
        container.register(Engine.class, Engine.class);

        BasicRestaurant basicRestaurant = (BasicRestaurant) container.newInstance(BasicRestaurant.class, 0, false);
        Bakery bakery = (Bakery) container.newInstance(Bakery.class, 0, false);
        assertNotSame(bakery.getFridge(), basicRestaurant.getFridge());

    }

    @Test
    @DisplayName("Test multiple tag injection")
    void testMultipleTagInjection() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        container.register(Engine.class, Engine.class);

        Restaurant restaurant = (Restaurant) container.newInstance(Restaurant.class, 0, false);
        assertTrue(restaurant.getFridge() instanceof Fridge);
        assertTrue(restaurant.getFurnace() instanceof Furnace);

    }

    @Test
    @DisplayName("Test recursive injection")
    void testRecursiveInjection() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        container.register(Equipment.class, Fridge.class);
        container.register(Engine.class, Engine.class);

        Bakery bakery = (Bakery) container.newInstance(Bakery.class, 0, false);
        assertNotNull(((Fridge) bakery.getFridge()).getEngine());

    }
}
