# Java Pro - Dependency Injector TP

## Class diagram
![alt_text](https://gitlab.isima.fr/bajuglair/DI-Container/-/raw/7134d9b58aa2aadd2a73bc68deafa12b1861c596/class_diagram.png)

The BasicRestaurant, Bakery and Restaurant classes use the Annotations @Autowired, @Unique and @Tag to represent different scenario for the container to handle.


## Annotations

### Autowired

The Autowired annotation is used to mark a field as injectable.

### Unique

The Unique annotation is used to mark an injectable field that must be instanciated without beeing reused.

### Tag

The tag annotation is used to define what class should be instanciated for this field.
